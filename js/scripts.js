// $(document).ready(function(){
//     $('[data-toggle="tooltip"]').tooltip();
// });
$(document).ready(function() {
  // Carousel
  $('#carouselButton').click(function() {
    if (
      $('#carouselButton')
        .children('span')
        .hasClass('fa-pause')
    ) {
      $('#mycarousel').carousel('pause');
      $('#carouselButton')
        .children('span')
        .removeClass('fa-pause');
      $('#carouselButton')
        .children('span')
        .addClass('fa-play');
    } else if (
      $('#carouselButton')
        .children('span')
        .hasClass('fa-play')
    ) {
      $('#mycarousel').carousel('cycle');
      $('#carouselButton')
        .children('span')
        .removeClass('fa-play');
      $('#carouselButton')
        .children('span')
        .addClass('fa-pause');
    }
  });

  // Reserve Link for Reserve table modal
  $('#reserveLink').click(function() {
    $('#reserveModal').modal('toggle');
  });

  // Login link for Login Modal
  $('#loginLink').click(function() {
    $('#loginModal').modal('toggle');
  });
});
